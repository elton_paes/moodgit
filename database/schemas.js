var mongoose = require('mongoose');
var Schema = mongoose.Schema;

exports.registerSchemas = function(req, res){
	
	//Users
	var users = new Schema({
		login: {type: String, required: true, unique: true},
		password: {type: String, required: true},
		email: {type: String},
		bucketName: {type: String, 'default': ''},
		firstAccess: {type: Boolean, 'default': true}
	});
	mongoose.model('Users', users);
	
	var MusicSchema = new Schema({
		name: {type: String},
	    title: {type: String},
	    author: {type: String},
	    album: {type: String},
	    year: {type: String},
	    genre: [{type: String}],
	    tags: [{type: String}],
	    owner: {type: String},
	    plays: {type: Number, 'default': 0},
	    uploaded: {type: Number, 'default': 0} // 0: uploading - 1: Uploaded - 2: Error
	});
	mongoose.model('Music', MusicSchema);
	
	
	var MusicCtrlSchema = new Schema({
	    title: {type: String},
	    author: {type: String},
	    album: {type: String},
	    genre: [{type: String}],
	    year: {type: String},
	    rate: {type: Number, 'default': 0},
	    rates: {type: Number, 'default': 0},
	    tags: [{type: String}]
	});
	mongoose.model('MusicCtrl', MusicCtrlSchema);
	
	console.log('Database is open');
	
};

exports.populate = function(req, res){
	var UserSchema  = mongoose.model('Users');
	
	var users_data = {
			login: 'epaes90',
			name: 'Elton',
			password: 'Flux@123',
			email: 'epaes90@gmail.com',
		};
	var user = new UserSchema(users_data);
	user.save(saveCallback);
	
	var users_data2 = {
			login: 'welton',
			name: 'Welton',
			password: 'Flux@123',
			email: 'wepaes93@gmail.com',
		};
	var user2 = new UserSchema(users_data2);
	user2.save(saveCallback);
	
	var japa = {
			login: 'ffuji',
			name: 'Welton',
			password: '123456',
			email: 'wepaes93@gmail.com',
		};
	var user3 = new UserSchema(japa);
	user3.save(saveCallback);
	
	var sergio = {
			login: 'sergio',
			name: 'Welton',
			password: '123456',
			email: 'wepaes93@gmail.com',
		};
	var user4 = new UserSchema(sergio);
	user4.save(saveCallback);
};

function saveCallback (error, data) {
    if (error) {
        //res.json(error);
        console.log('fail to save ' + error);
    } else {
        console.log("Added Seccessfully" + data);
        //res.statusCode = 201;
        //res.send();
    }
}