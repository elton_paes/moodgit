
/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var user = require('./routes/user');
var database = require('./database/schemas');
var http = require('http');
var path = require('path');
var engine = require('ejs-locals');
var fs = require('fs');

var app = express();

var mongoose = require('mongoose');
mongoose.connect('mongodb://nodejitsu:89657d839861a447a9c015b3056f206b@troup.mongohq.com:10086/nodejitsudb2722144289');
//mongodb://nodejitsu:89657d839861a447a9c015b3056f206b@troup.mongohq.com:10086/nodejitsudb2722144289
//mongodb://localhost/fluxjs

// all environments
app.set('port', process.env.PORT || 3001);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(express.bodyParser());
app.use(express.cookieParser('ASDQWER123'));
app.use(express.session());
app.use(app.router);
app.use(require('stylus').middleware(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));

app.engine('ejs', engine);
app.locals({
	_layoutFile:'layout.ejs'
});
// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/users', user.list);



var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function callback () {	
	
	//*schemas.js
	database.registerSchemas();
	//*populate database
	//database.populate();
	
	//dynamically include routes (Controller)
	fs.readdirSync('./controllers').forEach(function (file) {
	  if(file.substr(-3) == '.js') {
	      route = require('./controllers/' + file);
	      route.controller(app);
	  }
	});
});



http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});