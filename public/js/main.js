
var arrMusics = [];
var playlist = [];
var ind = -1;
var files;
var playingMusic;
var tableHeader = '';
var spriteTimer;
var firstAccess;
/**
 * Programatically hides the Upload Modal
 */
function hideModal(){
	$('#uploadModal').modal('hide');
	$('body').removeClass('modal-open');
	$('.modal-backdrop').remove();
}
$('#songInput').on('change', function (event){
	files = event.target.files;
});

$('form').on('submit', function (event){
	event.stopPropagation();
	event.preventDefault();

	spriteTimer = setInterval(animateProcessing, 1500);

	var data = new FormData();
	var toUpload =[];
	for(var i = 0; i < files.length; i++){
		if(files[i].type.indexOf('audio') != -1){
			toUpload.push(files[i]);
		}
	}

	$.each(toUpload, function(key, value){
		data.append(key, value);
	});

	$.ajax({
		url: '/upload',
		type: 'POST',
		data: data,
		cache: false,
		dataType: 'json',
		processData: false,
		contentType: false,
		success: function(data){
			if(!data.error){
				updateMusics();
			}
		},
		error: function(data){
			alert('error');
		}
	});
	hideModal();
});

//Document keydown
$(document).keydown(function(e){
	if(e.target.tagName != "INPUT"){
		console.log(e.keyCode)
		e.stopPropagation();
		if(e.keyCode == 39){
			nextMusic();
		}
		if(e.keyCode == 37){
			previousMusic();
		}
		if(e.keyCode == 32){
			if(audio.paused){
				audio.play();
			}
			else{
				audio.pause();
			}
		}
	}  
});
//Document Ready
$(document).ready(function(){
	$( "#availableSongs" ).sortable({
		cancel: "tr.cancelSorting",
		connectWith: '.connectedSortable',
		update : function () { 
			var order = $('#playlist').sortable('toArray'); 
			console.log(order);
			updateMusicList(arrMusics);
		} 
	}).disableSelection();
	var removeIntent = false;
	$( "#playlist" ).sortable({
		cancel: "tr.cancelSorting",
		over: function () {
			removeIntent = false;
		},
		out: function () {
			removeIntent = true;
		},
		beforeStop: function (event, ui) {
			if(removeIntent == true){
				ui.item.remove();   
				var order = $('#playlist').sortable('toArray', 'id'); 
				console.log(order);
				getPlaylistByOrder(order);
			}
		},
		update : function () { 
			var order = $('#playlist').sortable('toArray', 'id'); 
			console.log(order);
			getPlaylistByOrder(order);
		} 
	}).disableSelection();

	$( "#autocomplete" ).autocomplete({
		source: '/getMusicsName' ,
		select: function(event ,ui){
			putMusicNext(ui.item.label);
		}
	});

	updateMusicsAndPlaylist();
	updatePlaylistsName();

	$('input[type=file]').bootstrapFileInput();
	$('.file-inputs').bootstrapFileInput();

	setBalloons();
	$('#btnLogout').showBalloon({contents: 'botão de logout', position: "bottom"});
	$('#btnUpload2').showBalloon({contents: 'Clique aqui para <br/><b>upload</b>', position: "bottom", tipSize: 40});
	$('#songInput').showBalloon({contents: '<b>Escolha</b> suas músicas', position: "bottom", offsetX: -120});
	$('#autocomplete').showBalloon({contents: '<b>Procure</b> músicas para colocar em sua playlist atual', position: "right"});

	$('#savePlaylist').showBalloon({contents: 'Nomeie sua playlist atual e <b>salve-a</b>', position: "right"});
	$('#playlist').showBalloon({contents: 'Sua <b>playlist</b> atual. arraste músicas pra cá para montar playlists', position: "right"});
	$('#audio').showBalloon({contents: 'O <b>player</b>', position: "right"});
	$('#btnNext').showBalloon({contents: '<b>Controles</b> do player', position: "right"});
	$('#playing').showBalloon({contents: '<b>Música</b> que está tocando agora', position: "left"});
	$('#playlistsBtn').showBalloon({contents: 'Nossas <b>playlists sugeridas</b><br> e suas playlists salvas', position: "left"});
	$('#availableSongs').showBalloon({contents: 'Todas suas <b>músicas carregadas</b>.<br/> Arraste músicas pra Playlists<br/> para criá-las.', position: "left"});

	/**
	 * audio events
	 */

	$('#audio').on('play', function(){
		$.ajax({
			type: 'POST',
			url: '/getTags/',
			data: {name: playingMusic},
			success: function(data){
				$('#tags').val('');
				if(data != undefined && data.length > 0){
					data.forEach(function(item){
						$('#tags').val($('#tags').val() + item);
					})
				}
			}
		});
		$.ajax({
			type: 'POST',
			url: '/addPlayCount/',
			data: {name: playingMusic},
			success: function(data){
			}
		});
		getRate();
	});


	$('#audio').on('ended', function(){
		nextMusic();
	});

	$.ajax({
		url: '/isFirstAccess',
		type: 'POST',
		success: function(data){
			firstAccess = data.res;
		},
		error: function(data){
			alert('error');
		}
	});
});
var once = true;
$(document).click(function(e){
	if(once){
		$('#btnLogout').hideBalloon();


		$('#btnUpload2').hideBalloon();
		$('#songInput').hideBalloon();
		$('#autocomplete').hideBalloon();

		$('#savePlaylist').hideBalloon();
		$('#playlist').hideBalloon();
		$('#audio').hideBalloon();
		$('#btnNext').hideBalloon();
		$('#playing').hideBalloon();
		$('#playlistsBtn').hideBalloon();
		$('#availableSongs').hideBalloon();

		once = false;
	}
});

function setBalloons(){
	$('#btnLogout').balloon({contents: 'botão de logout', position: "bottom"});
	$('#btnUpload2').balloon({contents: 'Clique aqui para <br/><b>upload</b>', position: "bottom"});
	$('#songInput').balloon({contents: '<b>Escolha</b> suas músicas', position: "bottom", offsetX: 120});
	$('#autocomplete').balloon({contents: '<b>Procure</b> músicas para colocar em sua playlist atual', position: "right"});

	$('#savePlaylist').balloon({contents: 'Nomeie sua playlist atual e <b>salve-a</b>', position: "right"});
	$('#playlist').balloon({contents: 'Sua <b>playlist</b> atual. arraste músicas pra cá para montar playlists', position: "right"});
	$('#audio').balloon({contents: 'O <b>player</b>', position: "right"});
	$('#btnNext').balloon({contents: '<b>Controles</b> do player', position: "right"});
	$('#playing').balloon({contents: '<b>Música</b> que está tocando agora', position: "left"});
	$('#playlistsBtn').balloon({contents: 'Nossas <b>playlists sugeridas</b><br> e suas playlists salvas', position: "left"});
	$('#availableSongs').balloon({contents: 'Todas suas <b>músicas carregadas</b>.<br/> Arraste músicas pra Playlists<br/> para criá-las.', position: "left"});
}

var animateSprite = 0;
function animateProcessing(){
	if(animateSprite > 4)
		animateSprite = 0;
	switch(animateSprite){
	case 0:
		$('#processing').html('<b>Processando.<b>');
		break;
	case 1:
		$('#processing').html('<b>Processando..   <b>');
		break;
	case 2:
		$('#processing').html('<b>Processando...  <b>');
		break;
	case 3:
		$('#processing').html('<b>Processando.... <b>');
		break;
	case 4:
		$('#processing').html('<b>Processando.....<b>');
		break;
	}
	animateSprite++;
}

function getPlaylistByOrder(order){
	$.ajax({
		url: '/getPlaylistUpdate',
		type: 'POST',
		data: {pl: order},
		success: function(data){
			if(!data.status){
				updateMusicsFromDrag(data);
			}
			else
				console.log(data);
		}
	});
}

function saveTags(){
	var moods = $('#tags').val();
	$.ajax({
		type: 'POST',
		url: '/saveTags',
		data: {tags: moods,name: playingMusic},
		success: function(data){
			console.log(data);
		}
	});
}

function getPlaylist(att, value){
	$.ajax({
		type: 'POST',
		url:'/getPlaylist',
		data: {att: att, value: value},
		success: function(data){
			if(!data.status){
				playlist = data;
				ind = -1;
				updatePlaylist();
			}
		}
	})
}

function putMusicNext(song){
	var array = song.split(' - ');
	var title = array[0];
	var author = array[1];
	$.ajax({
		type: 'POST',
		url: '/getMusic',
		data: {title: title, author: author},
		success: function(data){
			playlist.splice(ind+1,0,data);
			updatePlaylist();
		}
	})
}

function updatePlaylist(){
	$('#playlist').html('');
	var i = 0;
	playlist.forEach(function(item){
		$('#playlist').html($('#playlist').html() + '<tr id="' + item.name.split('.')[0] + '"><td>' + item.title +'</td><td>'+ item.author +'</td><td>'+ item.year + '</td><td>'+ item.album + '</td></tr>');
		i++;
	});

}

var timer = undefined;

function nextMusic(){
	ind++;
	if(timer)
		clearTimeout(timer);
	if(ind >= playlist.length){
		ind = 0;
	}
	saveTags();
	playingMusic = playlist[ind].name;

	$('#playing').html('<b>' + playlist[ind]. title + '</b> - ' + playlist[ind].author);

	timer = setTimeout(changeStream, 500);
}

function previousMusic(){
	ind--;

	if(timer)
		clearTimeout(timer);
	if(ind < 0){
		ind = playlist.length - 1;
	}
	saveTags();
	playingMusic = playlist[ind].name;

	$('#playing').html('<b>' + playlist[ind]. title + '</b> - ' + playlist[ind].author);

	timer = setTimeout(changeStream, 500);
}

function changeStream(){
	console.log('changeStream');
	if(timer){
		clearTimeout(timer);        
	}
	$.ajax({
		url: '/getMusicUrl',
		type: 'POST',
		data: {name: playingMusic},
		success: function(data){
			if(!audio.paused){
				audio.autoplay= 'autoplay';
			}
			else{
				audio.autoplay= '';
			}
			audio.src = data.url;
		},
		fail: function(data){
			console.log(data);
		}
	});
}

function getRate(){
	if(playingMusic && playingMusic != ''){
		$.ajax({
			type: 'POST',
			url: '/getRate',
			data: {name: playingMusic},
			success: function(data){
				if(data && data.rate){
					console.log('rate:'+data.rate);
					$('#spRate').html(' ' + data.rate);	
					$('#spVotes').html(' ' + data.rates);	
				}
			},
			error : function(err){
				console.log('Erro: ' + err)
			}
		})
	}
}

function rating(sel){
	if(playingMusic && playingMusic != '' && sel.value != 0){
		$.ajax({
			type: 'POST',
			url: '/rateMusic',
			data: {name: playingMusic, rate: sel.value},
			success: function(data){
				console.log('rate:'+data.rate);
				$('#spRate').html(' ' + data.rate);
				$('#spVotes').html(' ' + data.rates);
			},
			error : function(err){
				console.log('Erro: ' + err)
			}
		})
	}
	else{
		sel.value = 0;
	}

}
function updateMusics(){
	$.ajax({
		type: 'POST',
		url: '/getMusics/',
		success: function(data){
			console.log('success');
			updateMusicList(data);
		},
	});
}

function updateMusicsAndPlaylist(){
	$.ajax({
		type: 'POST',
		url: '/getMusics/',
		success: function(data){
			console.log('success');
			updateMusicList(data);
			updatePlaylistFromBackend(data);
		},
	});
}

var flag = false;
function updateMusicList(data){
	arrMusics = [];
	var needUpload = false;
	if(!data.status){
		$('#availableSongs').html(tableHeader);
		var i = 1;
		data.forEach(function(item){
			arrMusics.push(item);
			$('#availableSongs').html($('#availableSongs').html() + '<tr id="' + item.name.split('.')[0] + '"><td id="songName-' + i + '">' + item.title +'</td><td>'+ item.author +'</td><td>'+ item.year + '</td><td>'+ item.album + '</td></td><td>'+ item.plays + '</td></tr>');
			if(item.uploaded == 0){
				needUpload = true;
				flag = true;
				$('#songName-' + i).html($('#songName-' + i).html() + ' <b>carregando...</b>');
				$('#' + item.name.split('.mp3')[0]).addClass('cancelSorting');
			}
			i++;
		});

		//Update Playlist buttons
		updatePlaylistsName();

		if(needUpload)
			verifyMusicStatus();

		if(firstAccess && flag && !needUpload){
			flag = false;
			$('#helpDragModal').modal('toggle');
		}
	}
}

function verifyMusicStatus(){
	$.ajax({
		type: 'POST',
		url: '/getMusicsStatus/',
		success: function(data){
			if(data.needUpload){
				setTimeout(verifyMusicStatus, 2000);
			}
			else{
				updateMusics();
				if(spriteTimer)
					clearInterval(spriteTimer);
				$('#processing').html('');
			}
		},
	});
}

function updatePlaylistFromBackend(data){
	if(!data.status){
		playlist = [];
		data.forEach(function(item){
			playlist.push(item);
		});
		updatePlaylist();
		ind = -1;
		nextMusic();
	}
}

function updateMusicsFromDrag(data){
	if(!data.status){
		var lengthBefore = playlist.length;
		playlist = [];
		var flag = true;
		data.forEach(function(item){
			playlist.push(item);
		});
		updateInd();
		updatePlaylist();
		if(playlist.length == 1 && lengthBefore == 0){
			nextMusic();
		}
	}
}

function updateInd(){
	console.log(playingMusic);
	ind = 0;
	for(var i = 0; i < playlist.length; i ++){
		if(playlist[i].name == playingMusic){
			ind = i; //+ 1;
		}
	}
}

function savePlaylist(){
	console.log('savePlaylist')
	var order = $('#playlist').sortable('toArray');
	if($('#tagName').val() != ''){
		var tag = $('#tagName').val();
		$.ajax({
			url: '/savePlaylist',
			type: 'POST',
			data: {pl: order, tag: tag},
			success: function(data){
				if(data.status == 0)
					$('#tagName').val('');
				updatePlaylistsName();
			}

		})  
	}
}
var allplaylists = [];
function updatePlaylistsName(){
	$.ajax({
		url: '/getPlaylistsName',
		data: {keep: true},
		type: 'POST',
		success: function(data){
			console.log(data);
			if(data){
				allplaylists = data;
				// $('#playlistsBtn').append('</p>');
			}
		}

	});
}

function openSecondView(){
	$('#songsTable').hide();
	$('#playlistsAvailable').show();
	$('#songsAvailable').show();
}

function openFirstView(){
	$('#songsTable').show();
	$('#playlistsAvailable').hide();
	$('#songsAvailable').hide();
}

function showPlaylistInList(playlist){
	var firstTime = true;
	if(allplaylists){
		openSecondView();
		$("#divLists").html('');
		allplaylists.forEach(function(item){
			
			if(item.att == playlist){
				if(firstTime){
					$('#listHeading').html(item.name);
					updateSongsAvailable(item.name);
					firstTime = false;
				}
				$("#divLists").append('<tr style="cursor:pointer;" ><td>' + '</td><td onclick="updateSongsAvailable(\'' + item.name + '\')">'+ item.name + '</td></tr>');
			}
		})
	}
	else{
		updatePlaylistsName();
		setTimeout(showArtistsPlaylist, 1000);
	}
}

function updateSongsAvailable(name){
	//alert(name);
	$('#listHeading').html(name);
	allplaylists.forEach(function(item){
		if(item.name == name){
			$('#avSongs').html('');
			item.playlist.forEach(function(song){
				$('#avSongs').append('<tr ><td >' + song.title +' <button class="btn btn-default btn-xs" onclick="putMusicNext(\'' + song.title + " - " + song.author + '\')">play</button></td><td>' + song.album + '</td><td>'+ song.year + '</td></td><td>'+ song.plays + '</td></tr>')
			});
		}
	});
}