var fs = require('fs');
var crypto = require('crypto');
var path = require('path');
var util = require('util');
var mongoose = require('mongoose');
var aws = require('aws-sdk');
var ip = require('ip');
var query = require('array-query');

var MusicSchema = mongoose.model('Music');
var MusicCtrlSchema = mongoose.model('MusicCtrl');
var UserSchema = mongoose.model('Users');

var rs = {};
var aws3 = {};

module.exports.controller = function(app) {


	/**
	 * a home page route
	 */
	app.get('/', checkAuth, function(req, res) {
		aws.config.loadFromPath('./AwsConfig.json');
		console.log(req.acceptedLanguages);
		MusicSchema.find({owner: req.session.user.login}, function(error, data) {
			if(!error && data != undefined && data.length > 0){

				data = shuffle(data);
				var music = data[0];
				playlists = buildplaylists(data);
				
				var firstAccess = true;
				if(req.session.user.firstAccess !== undefined){
					firstAccess = req.session.user.firstAccess;
					if(req.session.user.firstAccess){
						UserSchema.update({login: req.session.user.login}, {$set:{firstAccess: false}}, function(err, upData){
						});
						req.session.user.firstAccess = false;
					}
				}
				else{
					UserSchema.update({login: req.session.user.login}, {$set:{firstAccess: false}}, function(err, upData){
					});
					req.session.user.firstAccess = false;
				}
				
				res.render('../views/index.ejs', {title:'moodplayer <small>beta</small>', 
					musictitle:  'http://' + ip.address() + ':3001/stream/' + music.name, 
					musics: data, 
					playlists: playlists,
					firstAccess: firstAccess,
					ip: ip.address()});
			}
			else{
				var firstAccess = true;
				if(req.session.user.firstAccess){
					firstAccess = req.session.user.firstAccess;
					UserSchema.update({login: req.session.user.login}, {$set:{firstAccess: false}}, function(err, upData){
					});
					req.session.user.firstAccess = false;
				}
				res.render('../views/index.ejs', {title:'moodplayer beta', 
					musictitle:  '', 
					musics: '', 
					playlists: undefined,
					firstAccess: firstAccess,
					ip: ip.address()});
			}
		});


	});

	app.post('/getMusics', checkAuth, function(req, res){
		MusicSchema.find({owner: req.session.user.login}, function(error, data){
			if(data != undefined && data.length > 0){
				var musics = [];
				data.forEach(function(item){
					musics.push({title: item.title, 
						author: item.author, 
						name: item.name,
						url: item.url, 
						year: item.year, 
						album: item.album,
						plays: item.plays,
						uploaded: item.uploaded});
				});
				res.send(musics);
			}
			else{
				if(error)
					res.send({status: 1, details: error});
				else
					res.send({status: 2, details: 'data empty or invalid'})
			}
		}); 
	});
	app.post('/getMusicsStatus', checkAuth, function(req, res){
		MusicSchema.find({owner: req.session.user.login}, function(error, data){
			if(data){
				var needUpload = false;
				data.forEach(function(item){
					if(item.uploaded == 0)
						needUpload = true;
				});
				
				res.send({needUpload: needUpload});
			}
		});
	});
	app.post('/getPlaylistsName', checkAuth, function(req,res){
		MusicSchema.find({owner: req.session.user.login}, function(error, data){
			if(data){
				var playlists = buildplaylists(data, req.body.keep);
				res.send(playlists);
			}
		});
	});
	
	app.post('/getPlaylistUpdate', checkAuth, function(req, res){
		MusicSchema.find({owner: req.session.user.login}, function(error, data){
			if(data && data.length > 0){
				var playlist= [];
				var order = req.body.pl;

				for(var i =0; i < order.length; i++){
					var m = getMusicByName(data, order[i]);
					if(m)
						playlist.push(m);
				}
				console.log(playlist);
				res.send(playlist);
			}
			else{
				res.send({status: 1, details: 'data undefined'});
			}
		});
	});

	app.post('/getMusicUrl', checkAuth, function(req, res){
		if(req.session.user.bucketName != ''){
			s3 = new aws.S3();

			var params = {Bucket: req.session.user.bucketName, Key: req.body.name, Expires: 30};
			var url = s3.getSignedUrl('getObject', params);
			res.send({url: url});
		}

	});

	app.get('/getMusicsName', checkAuth, function(req, res){
		MusicSchema.find({owner: req.session.user.login, uploaded: 1}, function(error, data){
			if(data != undefined && data.length > 0){
				var musics = [];
				data.forEach(function(item){
					if(item.title.toLowerCase().indexOf(req.query.term.toLowerCase()) != -1
							|| (item.author && item.author.toLowerCase().indexOf(req.query.term.toLowerCase()) != -1))
						musics.push(item.title + ' - ' + item.author);
				});
				res.send(musics);
			}
			else{
				if(error)
					res.send({status: 1, details: error});
				else
					res.send({status: 2, details: 'data empty or invalid'});
			}
		}); 
	});


	app.get('/music/:name', checkAuth, function(req, res) {
		var likes = 0;
		console.log('music name: ' + req.params.name);
		MusicSchema.find({name: req.params.name, owner: req.session.user.login}, function(error, data) {
			console.log('data: ' + data[0]);

			res.render('../views/index.ejs', {title:'moodplayer', 
				musictitle:  'http://' + ip.address() + ':3001/stream/' + req.params.name, 
				musics: '',
				ip: ip.address()});
		});

	});

	app.post('/getMusic', checkAuth, function(req, res) {
		var likes = 0;
		console.log('music name: ' + req.body.title);
		MusicSchema.find({title: req.body.title, author: req.body.author, owner: req.session.user.login}, function(error, data) {
			console.log('data: ' + data[0]);

			res.send(data[0]);
		});

	});

	app.post('/getRate', checkAuth, function(req, res) {
		console.log('music name: ' + req.body.name);
		MusicSchema.find({name: req.body.name, owner: req.session.user.login}, function(error, data) {
			if(data && data.length > 0){
				console.log('data: ' + data[0]);
				MusicCtrlSchema.find({title: data[0].title, author: data[0].author}, function(err, datactrl){
					if(datactrl && datactrl.length > 0){
						res.send({rate: parseFloat(datactrl[0].rate).toFixed(1), rates: datactrl[0].rates});
					}
				});
			}
		});

	});

	app.post('/rateMusic', checkAuth, function(req, res) {
		console.log('music name: ' + req.body.name);
		MusicSchema.find({name: req.body.name, owner: req.session.user.login}, function(error, data) {
			if(data && data.length > 0){
				console.log('data: ' + data[0]);
				MusicCtrlSchema.find({title: data[0].title, author: data[0].author}, function(err, datactrl){
					if(datactrl && datactrl.length > 0){
						var avg = datactrl[0].rate;
						var count = datactrl[0].rates;
						if(count > 0){
							avg = (avg * count + parseFloat(req.body.rate))/(count + 1);
							count = count + 1;
						}
						else{
							avg = req.body.rate;
							count = 1;
						}
						res.send({rate: parseFloat(avg).toFixed(1), rates: count});
						MusicCtrlSchema.update({title: data[0].title, author: data[0].author}, {$set:{rate: avg, rates: count}}, function(error, dataUpd){
							if(error)
								console.log(error);
							console.log(dataUpd);
						});
					}
				});
			}
		});

	});

	app.get('/playlist/:att/:value', checkAuth, function(req, res) {
		MusicSchema.find({owner: req.session.user.login}, function(error, data) {
			if(req.params.att == 'genre' || req.params.att == 'tags'){
				var musics = query(req.params.att).has(req.params.value).on(data);  

			}
			else{
				var musics = query(req.params.att).is(req.params.value).on(data);  
			}

			var musicsName = [];

			musics.forEach(function(item){
				musicsName.push(item.name);
			});

			musicsName = shuffle(musicsName);

			res.render('../views/index.ejs', {title:'moodplayer', 
				musictitle:  'http://' + ip.address() + ':3001/stream/' + musicsName[0], 
				musics: musicsName,
				ip: ip.address()});
		});

	});
	app.post('/addPlayCount', checkAuth, function(req, res) {
		MusicSchema.find({name: req.body.name, owner: req.session.user.login}, function(error, data) {
			if(data && data.length > 0){
				var count = data[0].plays;
				if(count == undefined)
					count = 0;
				count ++;
				MusicSchema.update({name: req.body.name}, {$set:{plays: count}}, function(error, data){
					res.send({status: 0});
				});
			}
			else{
				console.log('data undefined');
			}
		});
	});
	app.post('/savePlaylist', checkAuth, function(req, res) {
		MusicSchema.find({owner: req.session.user.login}, function(error, data) {
			for(var i = 0; i < req.body.pl.length; i++){
				var m = getMusicByName(data, req.body.pl[i]);
				if(!m.tags)
					m.tags = [];

				m.tags.push(req.body.tag);

				MusicSchema.update({name: m.name}, {$set:{tags: m.tags}}, function(errUpMsc, dataUpMsc){
					if(errUpMsc)
						console.log(errUpMsc);
				});
				MusicSchema.find({title: m.title, author: m.author}, function(err, dataCtrl) {
					
					if(!dataCtrl[0].tags)
						dataCtrl[0].tags =[]
					
					dataCtrl[0].tags.push(req.body.tag);
					
					MusicCtrlSchema.update({title: dataCtrl[0].title, author: dataCtrl[0].author}, {$set:{tags: dataCtrl[0].tags}}, function(errUpCtrl, dataUpCtrl){
						if(errUpCtrl)
							console.log(errUpCtrl);
					});
				});
			}
			res.send({status: 0});
		});
	});
	app.post('/getPlaylist', checkAuth, function(req, res) {
		MusicSchema.find({owner: req.session.user.login}, function(error, data) {
			if(data != undefined && data.length > 0){
				var musics = [];
				if(req.body.att == 'genre' || req.body.att == 'tags'){
					musics = query(req.body.att).has(req.body.value).on(data);
				}
				else if(req.body.att == 'year'){
					var value = parseInt(req.body.value.split('s')[0]);
					console.log('value: ' + value);
					data.forEach(function(item){
						if(item.year != ''){
							var y = parseInt(item.year);
							var decade = Math.floor(y / 10) * 10;
							if(decade == value){
								musics.push(item);
							}
						}
					});
				}
				else{
					musics = query(req.body.att).is(req.body.value).on(data);  
				}

				musics = shuffle(musics);

				res.send(musics);  
			}
			else{
				res.send({status: 1, details: 'no musics to show'});
			}
		});

	});
	/**
	 * About page route
	 */
	app.get('/stream/:name', checkAuth, function(req, res) {
		console.log('stream/' + req.params.name);
		if(req.session.user.bucketName != ''){  

			var params = {Bucket: req.session.user.bucketName + '', Key: req.params.name + ''};
			//var file = fs.createWriteStream(path.join(__dirname, '../uploads/'));
			var s3 = new aws.S3();


			//console.log(data); 
			res.set('Content-Type', 'audio/mp3');
			aws3[req.session.user.login] = s3.getObject(params);

			var readStream = s3.getObject(params, function(err, data){
				console.log('s3 return:');
				console.log(data);
			}).createReadStream();

			rs[req.session.user.login] = readStream;

			readStream.pipe(res);

			readStream.on('data', function(chunk){
				console.log(chunk.length); 
			});

			readStream.on('error', function(err){
				console.log('error while piping ' + err);
				readStream.unpipe();
			});
			readStream.on('end', function(err){
				console.log('end piping ');
			});
		}
		else{
			console.log('no bucket Name');
			res.end();
		}
//		if(aws3[req.session.user.login] != undefined){
//		aws3[req.session.user.login].abort();
//		console.log('aborted');
//		}


//		var filePath = path.join(__dirname, '../uploads/' + req.params.name + '/' + req.params.name);
//		var stat = fs.statSync(filePath);

//		res.writeHead(200, {
//		'Content-Type': 'audio/mp3', 
//		'Content-Length': stat.size
//		});

//		var readStream = fs.createReadStream(filePath);
//		// We replaced all the event handlers with a simple call to util.pump()
//		util.pump(readStream, res);

	});

	app.post('/saveTags', checkAuth, function(req, res) {
		console.log(req.body.tags);
		console.log(req.body.name);

		if(req.body.tags != undefined && req.body.tags != ''){
			var tags = req.body.tags.split(',');
			MusicSchema.find({name: req.body.name}, function(error, data){
				if(data != undefined && data.length > 0){
					MusicCtrlSchema.find({title: data[0].title, author: data[0].author}, function(error, dataCtrl){
						var datatags = dataCtrl[0].tags;
						if(datatags != undefined && datatags.length > 0){
							tags.forEach(function(item){
								if(datatags.indexOf(item) == -1)
									datatags.push(item.trim());
							});

						}
						else{
							datatags = tags;
						}
						MusicCtrlSchema.update({title: dataCtrl[0].title, author: dataCtrl[0].author}, {$set:{tags: datatags}}, function(error, dataUpd){
							if(error)
								console.log(error);
							console.log(dataUpd);
							res.send({status: 0});
						});
						MusicSchema.update({name: data[0].name}, {$set:{tags: datatags}}, function(error, dataUpd){
							if(error)
								console.log(error);
							console.log(dataUpd);
						});
					});  
				}
			});
		}
		else{
			res.send({status: 0});
		}
	});

	app.post('/getTags', checkAuth, function(req, res) {
		var mood= '';
		console.log('getTags');
		console.log(req.body.name);
		MusicSchema.find({name: req.body.name}, function(error, data){
			if(data != undefined && data.length > 0){
				MusicCtrlSchema.find({title: data[0].title, author: data[0].author}, function(error, dataCtrl){
					console.log(dataCtrl);

					//Sync tags
					data[0].tags.forEach(function(item){
						if(dataCtrl[0].tags.indexOf(item) == -1){
							dataCtrl[0].tags.push(item);
						}
					});
					MusicCtrlSchema.update({title: data[0].title, author: data[0].author}, {$set:{tags: dataCtrl[0].tags}}, function(error, dataUpd){
						if(error)
							console.log(error);
						console.log(dataUpd);
					});

					dataCtrl[0].tags.forEach(function(item){
						if(data[0].tags.indexOf(item) == -1){
							data[0].tags.push(item);
						}
					});
					MusicSchema.update({name: data[0].name}, {$set:{tags: data[0].tags}}, function(error, dataUpd){
						if(error)
							console.log(error);
						console.log(dataUpd);
					});

					if(dataCtrl != undefined && dataCtrl.length > 0){
						var tags = dataCtrl[0].tags;

						if(tags != undefined){
							for(var i = 0; i < tags.length ;i++ ){
								if(i != 0){
									mood += ', ';
								}
								mood += tags[i];
							}

							res.send(mood);
						} 
					}
				});  
			}
		});
	});
	
	app.post('/isFirstAccess', checkAuth, function(req, res) {
		var firstAccess = req.session.user.firstAccess;
		if(firstAccess == undefined)
			firstAccess = true;
		
		res.send({res: firstAccess});
	})
};

function checkAuth(req, res, next) {
	if (!req.session.user) {
		res.redirect('/login');
	} else {
		next();
	}
}

function buildplaylists(data, keep1SongList){
	var playlists = [];
	data.forEach(function(item){

		//Author
		var author = query('name').is(item.author).on(playlists).pop();
		if(!author){
			var author = {name: item.author, att: 'author', playlist: []};
			author.playlist.push(item);
			playlists.push(author);
		}
		else{
			author.playlist.push(item);
		}

		//Year
		var y = parseInt(item.year);
		if(y != 'NaN' && item.year != ''){
			var decade = Math.floor(y / 10)*10;
			var year = query('name').is(decade.toString() + 's').on(playlists).pop();
			if(!year){
				year = {name: decade.toString() + 's', att: 'year', playlist: []};
				year.playlist.push(item);
				playlists.push(year);
			}
			else{
				year.playlist.push(item);
			}
		}


		//Genre
		item.genre.forEach(function(itemGenre){
			var genre = query('name').is(itemGenre).on(playlists).pop();
			if(!genre){
				var genre = {name: itemGenre, att: 'genre', playlist: []};
				genre.playlist.push(item);
				playlists.push(genre);
			}
			else{
				genre.playlist.push(item);
			}
		});

		//Tags
		item.tags.forEach(function(itemTag){
			var tag = query('name').is(itemTag).on(playlists).pop();
			if(!tag){
				var tag = {name: itemTag, att: 'tags', playlist: []};
				tag.playlist.push(item);
				playlists.push(tag);
			}
			else{
				tag.playlist.push(item);
			}
		});

	});
	if(!keep1SongList){
		var toDelete = [];

		for(var i = 0; i < playlists.length; i++){
			if(playlists[i].playlist.length == 1){
				toDelete.push(playlists[i]);
			}
		}
		for(var j = 0; j < toDelete.length; j++){
			playlists.splice(playlists.indexOf(toDelete[j]), 1);
		}

		playlists = query().sort("att").desc().on(playlists);
		playlists.forEach(function(item){
			item.playlist = shuffle(item.playlist);
		});
	}
	return playlists;
}

function shuffle(list){
	for(var i = 0; i < list.length; i++){
		var ind = Math.floor((Math.random() * list.length -1 )+1);
		if(ind > list.length - 1)
			ind = list.length - 1; 
		var item = list[ind];
		list[ind] = list[i];
		list[i] = item;
	}
	return list;
}

function getMusicByName(list, name){
	if(name.indexOf('.mp3') == -1){
		name = name + '.mp3';
	}
	for(var i = 0; i < list.length; i++){
		if(list[i].name == name){
			return list[i];
		}
	}
	return undefined;
}

