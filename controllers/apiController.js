/**
 * API Controller
 */
var fs = require('fs');
var crypto = require('crypto');
var mm = require('musicmetadata');
var path = require('path');
var aws = require('aws-sdk');
var stream = require('streamifier');
var mongoose = require('mongoose');
var util = require('util');
var ip = require('ip');
var query = require('array-query');

var MusicSchema = mongoose.model('Music');
var MusicCtrlSchema = mongoose.model('MusicCtrl');
var UserSchema = mongoose.model('Users');

aws.config.loadFromPath('./AwsConfig.json');

module.exports.controller = function(app) {

	/**
	 * Upload songs through API
	 * body:{
	 * 	authData:{ email, password }
	 * 	files: file
	 * 	songData: { title, author, album, genre, year }
	 */
	app.post('/api/upload', checkAuth, function(req, res) {
		console.log('upload');
		if(!req.files || !req.files.length <= 0) {
			res.send({status: 2, details: 'no file'});
		}
		var count = 0;
		var s3 = new aws.S3({params: {Bucket: req.body.user.bucketName + ''}});
		console.log(req.body.user.login);

		for(var name in req.files){
			count++;
			var ext = path.extname(req.files[name].originalFilename);
			if(ext.toLowerCase() == '.mp3' || ext.toLowerCase() == '.ogg'){
				console.log(req.files[name].path);
				var data = fs.readFileSync(req.files[name].path);


				var uid = crypto.randomBytes(10).toString('hex');
				var fileName = new String(uid + ext);

				amazonUpload(s3, data, fileName, req);
				persist(fileName, req);

			}
			else{
				console.log(req.files[name].path);
				console.log('extension not allowed');
			}

		}
		console.log(count);
		res.send({status: 0});
	});
	
};

function persist(fileName, req){
	console.log('persist');

	var music = {
			name: fileName,
			title : req.body.songData.title,
			author: req.body.songData.artist[0],
			album: req.body.songData.album,
			genre: req.body.songData.genre,
			year: req.body.songData.year,
			owner: req.body.user.login
	};

	if(music.title == ''){
		music.title = new String(req.files[name].originalFilename);
	}
	var musicSch = new MusicSchema(music);
	musicSch.save(function(error, data){
		console.log(data);
		if(!error){
			saveOnMusicCtrl(data);
		}
		else{
			console.log('error while saving: ' + error);
		}
	});
}

function saveOnMusicCtrl(data){
	
	MusicCtrlSchema.find({title: data.title, author: data.author}, function(errorCtrl, dataCtrl){
		console.log('***********************');
		console.log(data);
		if(dataCtrl == undefined || dataCtrl.length <= 0){
			var musicCtrl = {
					title : data.title,
					author: data.author,
					album: data.album,
					genre: data.genre,
					year: data.year
			};

			var musicCSch = new MusicCtrlSchema(musicCtrl);
			musicCSch.save(saveCallback);
		}
		else{
			if(dataCtrl[0].tags != undefined){
				var datatags = dataCtrl[0].tags;
				MusicSchema.update({name: data.name}, {$set:{tags: datatags}}, function(error, dataUpd){
					if(error)
						console.log(error);
					console.log(dataUpd);
				});
			}
		}
	});
}

function checkAuth(req,res,next){
	UserSchema.find({login: req.body.authData.email},function(error, data){
		if(data != undefined && data.length > 0){
			if(data[0].password == req.body.authData.password){
				next();
			}
			else{
				res.send({status: 1, details: 'User not logged'});
			}
		}
		else{
			res.send({status: 1, details: 'User not logged'});
		}
	});
}