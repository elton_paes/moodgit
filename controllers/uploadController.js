var fs = require('fs');
var crypto = require('crypto');
var mm = require('musicmetadata');
var path = require('path');
var aws = require('aws-sdk');
var stream = require('streamifier');
var mongoose = require('mongoose');
var MusicSchema = mongoose.model('Music');
var UserSchema = mongoose.model('Users');
var MusicCtrlSchema = mongoose.model('MusicCtrl');
aws.config.loadFromPath('./AwsConfig.json');
module.exports.controller = function(app) {

	/**
	 * a home page route
	 */
	app.post('/upload', checkAuth, function(req, res) {
		console.log('upload');
		if(!req.files || !req.files.length <= 0) {
			res.send({status: 2, details: 'no file'});
		}
		var count = 0;
		//aws.config.loadFromPath('./AwsConfig.json');
		var s3 = new aws.S3({params: {Bucket: req.session.user.bucketName + ''}});
		console.log(req.session.user.login);

		for(var name in req.files){
			count++;
			var ext = path.extname(req.files[name].originalFilename);
			if(ext.toLowerCase() == '.mp3' || ext.toLowerCase() == '.ogg'){
				console.log(req.files[name].path);
				var data = fs.readFileSync(req.files[name].path);


				var uid = crypto.randomBytes(10).toString('hex');
				var fileName = new String(uid + ext);

//				var dir = __dirname + "/../uploads/" + fileName;
//				fs.mkdirSync(dir, '0777');
//				fs.writeFileSync(dir + "/" + fileName, data);

				amazonUpload(s3, data, fileName, req);

				parseMetadataAndPersist(fileName, data, req.session.user.login, req.files[name].originalFilename);



			}
			else{
				console.log(req.files[name].path);
				console.log('extension not allowed');
				//continue;
			}

		}
		console.log(count);
		res.send({status: 0});
	});

};

function parseMetadataAndPersist(fileName, buffer, login, originalName){
	console.log('parseMetadataAndPersist');
	var read = stream.createReadStream(new Buffer(buffer));
	var parser = mm(read);
	console.log(parser.metadata);
	// listen for the metadata event
	//parser.on('metadata', function (result) {
	//console.log(result);
	var music = {
			name: fileName,
			title : parser.metadata.title,
			author: parser.metadata.artist[0],
			album: parser.metadata.album,
			genre: parser.metadata.genre,
			year: parser.metadata.year,
			owner: login
	};

	if(music.title == ''){
		music.title = new String(originalName);
	}
	var musicSch = new MusicSchema(music);
	musicSch.save(function(error, data){
		console.log(data);
		if(!error){
			saveOnMusicCtrl(data);
		}
		else{
			console.log('error while saving: ' + error);
			//continue;
		}
	});
	//});

//	parser.on('done', function (err) {
//	if (err) console.log('error while parsing!' + err);
//	else console.log('done parsing');
//	//stream.destroy();
//	});
}
function amazonUpload(s3, data, fileName, req){
	if(!req.session.user.bucketName == ''){
		s3.putObject({
			Key: fileName + '',
			Body: data
		}, function(error, resp){
			if(!error){
				console.log('successfully uploaded!');
				MusicSchema.update({name: fileName}, {$set:{uploaded: 1}}, function(err, data){
					if(err)
						console.log(err);
				});
			}
			else{
				console.log('error while uploading! ' + error);
			}
		});
	}
}
function saveOnMusicCtrl(data){
	
	MusicCtrlSchema.find({title: data.title, author: data.author}, function(errorCtrl, dataCtrl){
		console.log('***********************');
		console.log(data);
		if(dataCtrl == undefined || dataCtrl.length <= 0){
			var musicCtrl = {
					title : data.title,
					author: data.author,
					album: data.album,
					genre: data.genre,
					year: data.year
			};

			var musicCSch = new MusicCtrlSchema(musicCtrl);
			musicCSch.save(saveCallback);
		}
		else{
			if(dataCtrl[0].tags != undefined){
				var datatags = dataCtrl[0].tags;
				MusicSchema.update({name: data.name}, {$set:{tags: datatags}}, function(error, dataUpd){
					if(error)
						console.log(error);
					console.log(dataUpd);
				});
			}
		}
	});
}
function saveCallback (error, data) {
	if (error) {
		//res.json(error);
		console.log('fail to save ' + error);
	} else {
		console.log("Added Seccessfully" + data);
		//res.statusCode = 201;
		//res.send();
	}
}
function checkAuth(req, res, next) {
	if (!req.session.user) {
		res.redirect('/login');
	} else {
		next();
	}
}