var fs = require('fs');
var crypto = require('crypto');
var path = require('path');
var http = require('http');
var util = require('util');
var mongoose = require('mongoose');
var UserSchema = mongoose.model('Users');
var aws = require('aws-sdk');

module.exports.controller = function(app) {

	app.get('/login', function(req, res){
		res.render('../views/login.ejs', {title:'Flux', musictitle:  ''});
	});

	app.post('/login', function(req, res){
		if(req.body.auther == undefined || req.body.auther == ''){
			UserSchema.find({login: req.body.inEmail.toLowerCase()},function(error, data){
				if(data != undefined && data.length > 0){
					if(data[0].password == req.body.inPassword){
						req.session.user = data[0];
						res.send({status: 0});
					}
					else{
						res.send({status: 1, details: 'User or password is invalid'});
					}
				}
				else{
					res.send({status: 1, details: 'User or password is invalid'});
				}
			});
		}
		else{
			var data = {authToken: '6841b2ab6284d78f0e3c1170', token: req.body.auther}
			var dataString = JSON.stringify(data);
			var responseString = '';
			var headers = {
			  'Content-Type': 'application/json',
			  'Content-Length': dataString.length
			};
			var options = {
					host: '192.168.1.48',
					path: '/auth/',
					port: '3000',
					method: 'POST',
					headers: headers
			};

			callback = function(response) {
				
				response.on('data', function(data){
					responseString += data;
				});
				
				response.on('end', function() {
					var resultObject = JSON.parse(responseString);
					console.log(resultObject);
					if(resultObject.status == 0){
						UserSchema.find({login: resultObject.data.email.toLowerCase()},function(error, data){
							if(data != undefined && data.length > 0){
								req.session.user = data[0];
								res.send({status: 0});
							}
							else{
								res.send({status: 1, details: 'User or password is invalid'});
							}
						});
					}
					else{
						res.send({status: 2, details: 'Auther error, please use convencional method'});
					}
				});
				
			};

			var httpreq = http.request(options, callback);
			httpreq.write(dataString);
			httpreq.end();
		}

	});

	app.get('/logout', function(req, res){
		req.session.user = null;
		res.redirect('/login');
	});

	app.post('/newUser', function(req, res){
		console.log(req.body);
		UserSchema.find({login: req.body.inEmail.toLowerCase()},function(error, data){
			if(data == undefined || data.length == 0){

				var uid = crypto.randomBytes(10).toString('hex');
				var user_data = {
						login: req.body.inEmail,
						password: req.body.inPassword,
						email: req.body.inEmail,
						bucketName: uid,
						firstAccess: true
				};

				bucket = new aws.S3({params: {
					Bucket: uid + '', 
					Key: 'firstFile.txt', 
					CreateBucketConfiguration: {
						LocationConstraint: 'sa-east-1'
					}
				}
				});

				bucket.createBucket(function(err, resp){
					if(err){
						console.log(err);
						req.session.user.bucketName = '';
					}
					else{
						console.log('created Bucket');
						bucket.putObject({
							Body: 'This is the first file of ' + req.body.inEmail
						}, function(error, resp){
							if(!error){
								console.log('successfully created an uploaded!');
							}
							else{
								console.log('[createBucket] error while uploading! ' + error);
							}
						});
					}

				});

				var user = new UserSchema(user_data);
				user.save(function(err, data){
					if(err){
						console.log(err);
					}
					else{
						req.session.user = data;
						res.send({status: 0});
					}
				});
			}
			else{
				res.send({status: 1, details: 'Username already exists'});
			}
		});


	});
};



function saveCallback (error, data) {
	if (error) {
		//res.json(error);
		console.log('fail to save ' + error);
	} else {
		console.log("Added Seccessfully" + data);
		//res.statusCode = 201;
		//res.send();
	}
}